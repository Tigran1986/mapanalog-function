

function mapAnalog(callback, array){
    const newArray = [];
    for(let i = 0; i < array.length; i++){
        newArray.push(callback(array[i]));
    }
    return newArray;
}

const array1 = [1, 2, 3, 4, 5, 'rubo', null];
const array2 = mapAnalog(function (item) {
    return item / 2;
}, array1);

console.log(array2);